resource "libvirt_volume" "volume" {
  name   = var.volume_name
  source = var.volume_source
  pool   = var.pool_name
  format = var.format_type
}

output "volume_id" {
  value = libvirt_volume.volume.id
}
