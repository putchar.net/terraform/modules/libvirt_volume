variable "volume_name" {
  type = string
}

variable "volume_source" {
  type = string
}

variable "pool_name" {
  type = string
}

variable "format_type" {
  type    = string
  default = "qcow2"
}
